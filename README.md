## 1 RFP Template Repo

This Repo is designed to provide a basic template to use for a static internal dn&co website.

### 1.1 Lets Get Started!

In your terminal create a new website using this template:

- gatsby new gatsby-site https://bitbucket.org/Ben-Swindells/rfp_template/src/master/

replace gatsby-site with your website name

### Packages Used In This Template

- bootstrap
- gatsby-plugin-react-helmet
- gatsby-plugin-css-literal-loader
- hamburgers | https://jonsuh.com/hamburgers/
- slugify
- react-spring

## 2 File System

- public // Main build folder for the gatsby builder dont add files here add them to the static folder instead
- src // src for all functional components and scss files.
- . data // Place for data to be stored this includes large object data.
- . pages // Section for all page content.
- . components // Store any re usable components here, navbar, footers etc
- .. templates //Store template blocks here, panels, sliders etc.
- static // Place for images and other content public content.
- gatsby-config.js // Config file for website seo content.

### 2.1 Adding A New Template

Files follow a simple structure as seen below:

1. Open components folder then template folder.
2. Create a new folder in templates with the new template name, navbar, footer etc.
3. Place an index.js file to create the functionality.
4. Create a index.scss file to add styling to the template.
5. Your ready to go!

### 2.2 SCSS Structure

You'll find the main SCSS controller in components titled layout.scss use this file as the global controller and import any other SCSS files to this controller.

#### 2.2.1 Global Variables

You'll find the global variables in the layout.scss file, bare in mind this will effect the whole site. Dont worry about increasing font sizes as this is calculated to be reduced on other devices.

### 2.3 Image Structure.

Please place images in the static/images folder unless its a favcon.ico.

Place images in their related folders banners, logos etc.

If there is a new template add a new folder to reflect the new template.

## 3 Current Avaliable Templates

Below you'll see listed the templates that have been created and what they achieve.

### 3.1 Navbar

A basic navigation bar.

Params:

- links, these are added through the config.

### 3.2 Footer

A basic footer with logo and text support.

Params:

- No Current paramaters avaliable for this template.

### 3.3 Banner

A header banner to use at the top of a new page, includes various options.

Params:

- width, set a width for the image. The basic option will default to 100%
- height, set a height for the image. The basic option will default to 700px.
- imgUrl, set the image URL. Required.
- backgroundOn, use a background image rather than a static image. Defaults to static image.

### 3.4 Media Grid

A Media grid that can display images in a row with fixed columns.

Requirements to use:

1. Locate the /images/grids file path.
2. Create a new folder with the name of the grid e.g demo.
3. Add your images here with the name 1 for first image, 2, 3 and so on.

Params:

- location, location of the file path to the source images, for example: /images/grids/demo/.
- size, size of the grid, for example to display 3 images the size would need to be 3. Make sure that the grid size reflects the images/grid/ then your named folder.
- imgType, type of image that needs to be rendered for example .jpg or .png
- columns, bootstrap columns for example col-md-4 col-sm-6, a place to add classes to the columns.
- width, set a width for the image. The basic option will default to 100%
- height, set a height for the image. The basic option will default to 100%.

### 3.5 Ribbon

A media file that has no container / border around it.

Params:

- padding, put in your padding here this will effect the top and bottom borders e.g "50px"
- height, effect height of media.

### 3.6 Blocks

A Simple media block that can display either a image or video depending on the params.

Params:

- mediaSrc, the source of the media
- mediaType, can either be video, img or image.
- mediaClass, adds a class to the block normally a column would go here when in a bootstrap row.
- textPosition, if text is added to the block you can define its location e.g. top-left, bottom-left etc.

## 4 CSS Classes

### 5.1 Breakers

- .breaker | A simple underline with padding.
- .breaker-light | A simple line with less padding then the breaker.

### 5.2 Slices / Text width

- .slice-half | decreases text width by 50%
- .slice-third | decreases text width by 25%

## 5 Optional Features (Feel Free To Delete These Features)

- Context API for state management, please delete the states folder if not use. Or wrap your layout.js file with the Context file.
