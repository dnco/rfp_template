import React from "react";

const Banner = (props) => {
  const bannerColourStyles = {
    backgroundColor: props.colour ? props.colour : "black",
    height: props.bannerColourHeight ? props.bannerColourHeight : "700px",
    width: props.width ? props.width : "100%",
  };
  const bannerImageStyles = {
    height: props.height ? props.height : "auto",
    width: props.width ? props.width : "100%",
  };
  const bannerBackgroundImageStyles = {
    backgroundImage: `url(${props.mediaBackgroundImageSrc})`,
    height: props.bannerBackgroundImageHeight
      ? props.bannerBackgroundImageHeight
      : "700px",
    width: props.width ? props.width : "100%",
    backgroundRepeat: "no-repeat",
  };
  switch (props.mediaType) {
    case "background":
      return (
        <>
          <div className="banner-container" style={bannerColourStyles}>
            <div className="container banner-content mobile-hide">
              {props.children}
            </div>
          </div>
          <div className="container banner-content-mobile desktop-hide mobile-show">
            {props.children}
          </div>
        </>
      );
    case "img" || "image":
      return (
        <>
          <div className="banner-container" style={bannerImageStyles}>
            <img
              src={props.mediaImageSrc}
              height={props.height ? props.height : "auto"}
              width={props.width ? props.width : "100%"}
              alt={props.altText ? props.altText : "A dn&co graphic design"}
            />
            <div className="container banner-content mobile-hide">
              {props.children}
            </div>
          </div>
          <div className="container banner-content-mobile desktop-hide mobile-show">
            {props.children}
          </div>
        </>
      );
      break;
    case "background-img" || "background-image":
      return (
        <>
          <div
            className="banner-container"
            style={
              props.effect === "parallax"
                ? {
                    ...bannerBackgroundImageStyles,
                    backgroundAttachment: "fixed",
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                  }
                : bannerBackgroundImageStyles
            }
          >
            <div className="container banner-content mobile-hide">
              <div style={{ color: "black" }}>{props.children}</div>
            </div>
          </div>
          <div className="container banner-content-mobile desktop-hide mobile-show">
            {props.children}
          </div>
        </>
      );
      break;
    case "video":
      return (
        <>
          <div className="banner-container ">
            <video
              autoPlay
              muted
              loop
              playsnline
              style={{ display: "block", width: "100%", height: "auto" }}
            >
              <source src={props.mediaVideoSrc} type="video/mp4" />
            </video>
            <div className="container banner-content mobile-hide">
              {props.children}
            </div>
            <div className="container banner-content-mobile desktop-hide mobile-show">
              {props.children}
            </div>
          </div>
        </>
      );
      break;
    default:
      return (
        <>
          <div className="banner-container" style={bannerImageStyles}>
            {props.backgroundOn ? (
              <></>
            ) : (
              <img
                src={props.mediaImageSrc}
                height={props.height ? props.height : "auto"}
                width={props.width ? props.width : "100%"}
                alt={props.altText ? props.altText : "A dn&co graphic design"}
              />
            )}
            <div className="container banner-content mobile-hide">
              {props.children}
            </div>
          </div>
          <div className="container banner-content-mobile desktop-hide mobile-show">
            {props.children}
          </div>
        </>
      );
      break;
  }
};

export default Banner;
