import React from "react";

const Ribbon = (props) => {
  const ribbonStyles = {
    background: {
      backgroundImage: props.imgUrl ? `url(${props.imgUrl})` : ``,
      backgroundColor: props.bkColour,
      height: props.height,
      width: "100%",
      backgroundPosition: "center",
    },
    backgroundParallax: {
      backgroundImage: props.imgUrl ? `url(${props.imgUrl})` : ``,
      height: props.height,
      width: "100%",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundAttachment: "fixed",
    },
    container: {
      width: "100%",
      height: "auto",
      marginTop: props.margin ? props.margin : "50px",
      marginBottom: props.margin ? props.margin : "50px",
    },
    content: {
      paddingTop: props.padding ? props.padding : "50px",
      paddingBottom: props.padding ? props.padding : "50px",
    },
  };
  return (
    <div style={ribbonStyles.container}>
      <div
        style={
          props.backgroundType === "parallax"
            ? ribbonStyles.backgroundParallax
            : ribbonStyles.background
        }
      >
        <div
          style={ribbonStyles.content}
          className={
            props.noContainer ? "ribbon-content" : "ribbon-content container"
          }
        >
          {props.children}
        </div>
      </div>
    </div>
  );
};

export default Ribbon;
