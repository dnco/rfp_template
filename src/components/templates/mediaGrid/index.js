import React, { useEffect } from "react";

const MediaGrid = ({
  location,
  size,
  columns,
  imgType,
  height,
  width,
  itemSelect,
}) => {
  const items = [];
  useEffect(() => {
    console.log(itemSelect);
  }, []);

  for (var i = 1; i <= size; i++) {
    items.push(
      <div key={i} className={columns + " grid-gap"}>
        <img
          src={location + i + "." + imgType}
          height={height ? height : "100%"}
          width={width ? width : "100%"}
          alt="dnco media grid"
        />
      </div>
    );
  }
  return <div className="row">{items}</div>;
};
export default MediaGrid;
