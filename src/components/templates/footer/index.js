import React from "react";

export const FooterTemplate = () => {
  return (
    <div className="footer">
      <div className="container">
        <div className="footer-content">
          <img
            className="logo buffer"
            src="/images/logos/dn-logo.png"
            alt="dnco logo"
          />
          <p className="copyright">&copy; {new Date().getFullYear()}</p>
        </div>
      </div>
    </div>
  );
};

export default FooterTemplate;
