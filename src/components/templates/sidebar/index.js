import React from "react";

import { siteMetadata } from "../../../../gatsby-config";
import { animated } from "react-spring";
import { Link } from "gatsby";
import slugify from "slugify";
const Sidebar = ({ sidebarAnim }) => {
  return (
    <animated.div className="sidebar-container container" style={sidebarAnim}>
      <div className="sidebar-inner">
        {siteMetadata.links.map((link, id) => {
          var siftLink = slugify(link, { replacement: "", lower: true });
          return (
            <Link key={id} to={siftLink == "home" ? "/" : "/" + siftLink}>
              <h2>{link}</h2>
            </Link>
          );
        })}
      </div>
    </animated.div>
  );
};

export default Sidebar;
