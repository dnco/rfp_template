import React from "react";

const Panel = ({ children, mediaSrc, mediaType, height, textPosition }) => {
  const panelBKImage = {
    backgroundImage: `url(${mediaSrc})`,
    height: height ? height : "700px",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    backgroundRepeat: "no-repeat",
  };
  const textStyling = {
    textAlign: textPosition ? textPosition : "left",
  };
  switch (mediaType) {
    case "img":
      return (
        <div className="panel-container" style={panelBKImage}>
          <div className="container panel-content">{children}</div>
        </div>
      );
      break;
    case "image":
      return (
        <div className="panel-container" style={panelBKImage}>
          <div className="container panel-content">{children}</div>
        </div>
      );
      break;
    case "video":
      break;
    default:
      return (
        <div className="panel-container" style={panelBKImage}>
          <div className="container panel-content" style={textStyling}>
            {children}
          </div>
        </div>
      );
      break;
  }
};

export default Panel;
