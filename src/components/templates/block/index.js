import React from "react";

const Block = ({ children, mediaType, mediaSrc, mediaClass, textPosition }) => {
  switch (mediaType) {
    case "video":
      return (
        <video
          autoPlay
          muted
          loop
          playsnline
          className={mediaClass}
          style={{ display: "block", width: "100%", height: "auto" }}
        >
          <source src={mediaSrc} type="video/mp4" />
        </video>
      );
      break;
    case "img" || "image":
      return (
        <div className={"block-outer" + " " + mediaClass}>
          <img src={mediaSrc} width="100%" alt="block" />
          <div
            className={"block-text" + " " + textPosition ? textPosition : ""}
          >
            {children}
          </div>
        </div>
      );
      break;
    default:
      return (
        <div className="block-outer">
          <img src={mediaSrc} className={mediaClass} alt="block" />
          <div className={"block-text" + " " + textPosition}>{children}</div>
        </div>
      );
      break;
  }
};
export default Block;
