import React, { useContext, useEffect, useState } from "react";
import { ContextStore } from "../../states/context";
import { Link } from "gatsby";
import slugify from "slugify";
import { siteMetadata } from "../../../../gatsby-config";
import { useSpring } from "react-spring";
import Sidebar from "../sidebar";

export const Navbar = () => {
  const { state, dispatch } = useContext(ContextStore);
  const [rightActive, setSidebarActive] = useState(false);

  const rightMenuAnimation = useSpring({
    opacity: rightActive ? 1 : 0,
    transform: rightActive ? `translateX(0%)` : `translateX(-100%)`,
  });

  return (
    <>
      <nav className="navbar">
        <button
          onClick={() => {
            setSidebarActive(!rightActive);
            dispatch({ type: "ACTION_SIDEBAR" });
          }}
          className={
            state.sidebarActive
              ? "hamburger--collapse is-active"
              : "hamburger--collapse"
          }
          type="button"
        >
          <span className="hamburger-box">
            <span className="hamburger-inner"></span>
          </span>
        </button>
        <ul className="link-container">
          <li>
            {siteMetadata.links.map((link, id) => {
              var siftLink = slugify(link, { replacement: "", lower: true });
              return (
                <Link key={id} to={siftLink === "home" ? "/" : "/" + siftLink}>
                  {link}
                </Link>
              );
            })}
          </li>
        </ul>
      </nav>
      <Sidebar sidebarAnim={rightMenuAnimation} />
    </>
  );
};

export default Navbar;
