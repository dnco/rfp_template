//Context for context api, wrap this around layout for use, if not in use please delete states folder.

import React, { createContext, useReducer } from "react";
let initialState = {
  sidebarActive: false,
};
let reducer = (state, action) => {
  switch (action.type) {
    case "ACTION_SIDEBAR":
      return { ...state, sidebarActive: !state.sidebarActive };
      break;
    default:
      break;
  }
};

const ContextStore = createContext(initialState);

const ContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <ContextStore.Provider value={{ state, dispatch }}>
      {props.children}
    </ContextStore.Provider>
  );
};
export { ContextStore, ContextProvider };
