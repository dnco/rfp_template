import React, { useContext } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "hamburgers/dist/hamburgers.min.css";
import "./layout.scss";
import Header from "./header";
import Footer from "./footer";

import { ContextProvider } from "../components/states/context";
import { ContextStore } from "../components/states/context";

const Layout = ({ children }) => {
  return (
    <ContextProvider>
      <Header />
      <SidebarWrapper>
        {children}
        <Footer />
      </SidebarWrapper>
    </ContextProvider>
  );
};

//Wrapper for sidebar, shuts down components on mobile for sidebar full width.
const SidebarWrapper = ({ children }) => {
  const { state, dispatch } = useContext(ContextStore);
  return (
    <>
      <div className={state.sidebarActive ? "mobile-hide" : ""}>{children}</div>
    </>
  );
};
export default Layout;
