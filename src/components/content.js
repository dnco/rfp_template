import React from "react";

const Content = ({ children, isFluid, noBuffer }) => {
  return (
    <div className={isFluid ? "container-fluid" : "container"}>
      <div className={noBuffer ? "" : "buffer"}>{children}</div>
    </div>
  );
};

export default Content;
