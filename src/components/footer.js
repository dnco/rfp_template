import React from "react";
import FooterTemplate from "../components/templates/footer";

const Footer = () => {
  return (
    <footer>
      <FooterTemplate />
    </footer>
  );
};

export default Footer;
