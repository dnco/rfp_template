import React from "react";
import Navbar from "./templates/navbar";

const Header = () => {
  return (
    <header>
      <Navbar />
    </header>
  );
};

export default Header;
