import React from "react";
import SEO from "../components/seo";
import Layout from "../components/layout";
import Content from "../components/content";
import MediaGrid from "../components/templates/mediaGrid";
import Ribbon from "../components/templates/ribbon";
import Banner from "../components/templates/banner";

const Home = () => {
  return (
    <Layout>
      <SEO title="Google" />
      <Banner mediaSrc="/images/banners/page_1_banner.jpg" width="100%">
        <h1>Banner Heading</h1>
        <h2>I am a sub heading</h2>
      </Banner>
      <Content isFluid={false}>
        <h1>Heading 1</h1>
        <div className="breaker"></div>
        <h2>Heading 2</h2>
        <div className="breaker"></div>
        <h3>Heading 3</h3>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
        <p className="slice-third">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
        <p className="slice-half">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
        <MediaGrid
          location="/images/grids/demo/"
          size={2}
          columns="col-md-6"
          imgType="jpg"
        />
      </Content>
      <Ribbon imgUrl="/images/banners/page_1_banner.jpg">
        <h1>Heading</h1>
        <h2>Sub Title</h2>
        <p className="slice-half">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
      </Ribbon>
      <Content>
        <MediaGrid
          location="/images/grids/demo/"
          size={3}
          columns="col-md-4"
          imgType="jpg"
          itemSelect={{ id: 1, col: "col-md-6", media: "video" }}
        />
      </Content>
    </Layout>
  );
};

export default Home;
