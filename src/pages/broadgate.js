import React from "react";
import Layout from "../components/layout";
import Content from "../components/content";
import Ribbon from "../components/templates/ribbon";
import SEO from "../components/seo";
const Broadgate = () => {
  return (
    <Layout>
      <SEO title="Broadgate" />
      <Content>
        <div className="row">
          <div className="col-md-6">
            <video
              autoPlay
              muted
              loop
              playsnline
              style={{ display: "block", width: "100%", height: "auto" }}
            >
              <source
                src="https://dnco.com/media/projects/broadgate/180504_broadgate_20sec_loop_v3_16-9.mp4"
                type="video/mp4"
              />
            </video>
            <br></br>
            <h3 style={{ fontSize: "22px", textAlign: "right" }}>
              Creating the brand
            </h3>
            <p style={{ textAlign: "right" }}>
              Our dynamic identity reflects Broadgate's unique status as a
              wholly-pedestrianised neighbourhood — a place that energises
              people by connecting, empowering and surprising them.
            </p>
            <p style={{ textAlign: "right" }}>
              Taking inspiration from the life and forms of its public spaces,
              the brand is always in motion. The generative ‘kinetic B’ is
              endlessly expressive, changing shape and scale with a colour
              palette of interchangeable pairings of bright and dark tones. The
              result is a consumer-oriented, contemporary identity.
            </p>
          </div>
          <div className="col-md-6">
            <img
              src="/images/broadgate/tube.jpg"
              style={{ display: "block", width: "100%", height: "auto" }}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6"></div>
          <div className="col-md-6">
            <br></br>
            <p>Broadgate</p>
            <h3 style={{ paddingTop: "0" }}>
              Where innovation and finance play
            </h3>
            <h4>
              <span style={{ fontSize: "22px" }}>
                Connecting the City and Silicon Roundabout, this neighbourhood
                was perfectly placed but poorly perceived. We created a brand
                strategy and identity to capture the excitement around
                Broadgate's reinvention — asking people to imagine what could
                happen where innovation and finance play.
              </span>
            </h4>
          </div>
        </div>
      </Content>
      <Ribbon noContainer={true} padding="0px">
        <video
          autoPlay
          muted
          loop
          playsnline
          style={{ display: "block", width: "100%", height: "auto" }}
        >
          <source
            src="https://dnco.com/media/projects/broadgate/Broadgate-Kinetic-B-ident.mp4"
            type="video/mp4"
          />
        </video>
      </Ribbon>
    </Layout>
  );
};

export default Broadgate;
