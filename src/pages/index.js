import React, { useState, useCallback, useRef, useEffect } from "react";
import SEO from "../components/seo";
import Layout from "../components/layout";
import Content from "../components/content";
import MediaGrid from "../components/templates/mediaGrid";
import Ribbon from "../components/templates/ribbon";
import Banner from "../components/templates/banner";
import Block from "../components/templates/block";
import Panel from "../components/templates/panel";
import { useTransition, animated } from "react-spring";

const Home = () => {
  const ref = useRef([]);
  const [items, set] = useState([]);
  const transitions = useTransition(items, null, {
    from: {
      opacity: 0,
      height: 0,
      innerHeight: 0,
      transform: "perspective(600px) rotateX(0deg)",
      color: "#8fa5b6",
    },
    enter: [
      { opacity: 1, height: 80, innerHeight: 80 },
      { transform: "perspective(600px) rotateX(180deg)", color: "#28d79f" },
      { transform: "perspective(600px) rotateX(0deg)" },
    ],
    leave: [
      { color: "#c23369" },
      { innerHeight: 0 },
      { opacity: 0, height: 0 },
    ],
    update: { color: "#28b4d7" },
  });
  const reset = useCallback(() => {
    ref.current.map(clearTimeout);
    ref.current = [];
    set([]);
    ref.current.push(setTimeout(() => set(["Genesis"]), 2000));
  }, []);
  useEffect(() => void reset(), []);
  return (
    <Layout>
      <SEO title="Home" />
      <Banner
        mediaVideoSrc="/images/banners/page_1_banner_video.mp4"
        mediaType="video"
        width="100%"
      >
        <h1>Welcome to the demo Not Genesis</h1>
        <h2>Building websites fast and with efficiency </h2>
        <h3>Banner template</h3>
      </Banner>
      <Content isFluid={false}>
        <h1>Heading 1</h1>
        <div className="breaker"></div>
        <h2>Heading 2</h2>
        <div className="breaker"></div>
        <h3>Heading 3</h3>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
        <p className="slice-third">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
        <p className="slice-half">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
      </Content>
      <Content>
        <h1>Grids</h1>
        <br></br>
        <MediaGrid
          location="/images/grids/demo/"
          size={2}
          columns="col-md-6"
          imgType="jpg"
        />
        <MediaGrid
          location="/images/grids/demo/"
          size={3}
          columns="col-md-4"
          imgType="jpg"
          itemSelect={{ id: 1, col: "col-md-6", media: "video" }}
        />
        <br></br>
        <h1>Ribbons</h1>
      </Content>
      <Ribbon margin="0px" imgUrl="/images/banners/page_1_banner.jpg">
        <h1>Heading</h1>
        <h2>A static ribbon</h2>

        <p className="slice-half">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
      </Ribbon>
      <div className="container-fluid">
        <h1>Media Blocks</h1>
        <h3>Breaking out of the grid</h3>
        <br></br>
      </div>
      <div className="row no-gutters">
        <Block
          mediaClass="col-md-6"
          mediaType="img"
          mediaSrc="/images/blocks/demo/museum-home-1.jpg"
          textPosition="top-left"
        >
          <h3>Text top right</h3>
          <p>With sub header</p>
        </Block>
        <Block
          mediaClass="col-md-6"
          mediaType="img"
          mediaSrc="/images/blocks/demo/dnco-va-wayfinding-1.jpg"
          textPosition="bottom-right"
        >
          <h3>Text bottom right</h3>
          <p>With sub header</p>
        </Block>
        <Block
          mediaClass="col-md-8"
          mediaType="video"
          mediaSrc="/images/broadgate/broadgate-intro.mp4"
        ></Block>
        <div className="col-md-4 center-content">
          <div className="container">
            <h2>Content block</h2>
            <h3>Sub header</h3>
            <div className="breaker-thin"></div>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>
        </div>
      </div>

      <Ribbon margin="0px" bkColour="black">
        <h1>No background image ribbon</h1>
        <h2>Make me any colour!</h2>

        <p className="slice-half">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
      </Ribbon>
      <Ribbon margin="0px" bkColour="red">
        <h1>No background image ribbon</h1>
        <h2>Make me any colour!</h2>

        <p className="slice-half">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
      </Ribbon>
      <Ribbon
        imgUrl="/images/banners/page_1_banner.jpg"
        backgroundType="parallax"
        margin="0px"
      >
        <h1>Heading</h1>
        <h2>A ribbon with background effects</h2>

        <p className="slice-half">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
      </Ribbon>
      <div className="buffer"></div>
      <Content>
        <h1>Panels</h1>
      </Content>
      <div className="buffer"></div>
      <Panel mediaSrc="/images/demo/panels/panel-1.png">
        <h1>1. We bring purpose to places</h1>
        <h3>Left aligned is best...</h3>
        <br></br> <br></br> <br></br>
        <p className="slice-quarter">
          <b>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </b>
        </p>
      </Panel>
      <Panel mediaSrc="/images/demo/panels/panel-2.png" textPosition="right">
        <h1>2. We bring purpose to places</h1>
        <h3>No right aligned is best...</h3>
        <br></br> <br></br> <br></br>
        <p className="slice-quarter-right">
          <b>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </b>
        </p>
      </Panel>
      <div className="buffer"></div>
      <br></br>
      <div className="container">
        {transitions.map(({ item, props: { innerHeight, ...rest }, key }) => (
          <animated.div
            className="transitions-item"
            key={key}
            style={rest}
            onClick={reset}
          >
            <animated.div style={{ overflow: "visible", height: innerHeight }}>
              {item}
            </animated.div>
          </animated.div>
        ))}
      </div>
      <div className="buffer"></div>
    </Layout>
  );
};

export default Home;
