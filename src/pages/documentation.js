import React, { useState } from "react";
import Layout from "../components/layout";
import Content from "../components/content";
import { BlockPicker } from "react-color";
import MediaGrid from "../components/templates/mediaGrid";
import slugify from "slugify";

//Tempalates
import Banner from "../components/templates/banner";

const Documentation = () => {
  //Header Links
  const [headerLinks, setHeaderLinks] = useState([
    "Introduction",
    "Formatting",
    "Header Banner",
    "Grids",
  ]);

  // Banner Functions
  const [bannerType, setBannerType] = useState("");
  const [bannerColour, setBannerColour] = useState("black");
  const [bannerBackgroundEffect, setBackgroundEffect] = useState("");

  //Display States
  const [bannerDisplay, setBannerDisplay] = useState(false);
  const [bannerBackgroundDisplay, setBannerBackgroundDisplay] = useState(false);

  const handleChangeComplete = (color) => {
    setBannerColour(color);
  };

  return (
    <Layout>
      <div className="row no-gutters">
        <div className="col-md-10">
          <Content>
            <div id="introduction">
              <h1>Introduction</h1>
              <div className="breaker"></div>
            </div>
            <div id="formatting">
              <h1>Formatting</h1>
              <div className="breaker"></div>
              <h1>Heading 1</h1>
              <h2>Heading 2</h2>
              <h3>Heading 3</h3>
              <h4>Heading 4</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <p className="slice-third">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <p className="slice-half">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
            </div>
            <div id="headerbanner">
              <h1>Header Banner </h1>
              <div className="breaker"></div>
              <Banner
                mediaVideoSrc="/images/banners/page_1_banner_video.mp4"
                mediaImageSrc="/images/blocks/demo/museum-home-1.jpg"
                mediaBackgroundImageSrc="/images/blocks/demo/museum-home-1.jpg"
                mediaType={bannerType == "" ? "video" : bannerType}
                bannerColourHeight="523.33px"
                bannerBackgroundImageHeight="590px"
                height="590px"
                colour={bannerColour}
                width="100%"
                effect={bannerBackgroundEffect}
              >
                <Content>
                  <h1>Heading 1</h1>
                  <h2>Heading 2</h2>
                  <h3>Heading 3</h3>
                </Content>
              </Banner>
              <br></br>
              <div className="row">
                <div className="col-md-3">
                  <button
                    type="button"
                    style={{
                      width: "100%",
                      display: "inline-block",
                    }}
                    onClick={() => {
                      setBannerDisplay(true);
                      setBannerBackgroundDisplay(false);
                      setBannerType("background");
                    }}
                    className="btn btn-primary btn-documentation"
                  >
                    Fill Type
                  </button>
                  <div className="buffer"></div>
                  {bannerDisplay ? (
                    <BlockPicker
                      color={bannerColour}
                      value={bannerColour}
                      style={{ margin: "0 auto" }}
                      onChange={(e) => {
                        handleChangeComplete(e.hex);
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </div>

                <div className="col-md-3">
                  <button
                    type="button"
                    style={{ width: "100%" }}
                    onClick={() => {
                      setBannerType("video");
                      setBannerDisplay(false);
                      setBannerBackgroundDisplay(false);
                    }}
                    class="btn btn-primary btn-documentation"
                  >
                    Video Type
                  </button>
                </div>
                <div className="col-md-3">
                  <button
                    type="button"
                    style={{ width: "100%" }}
                    onClick={(e) => {
                      setBannerType("image");
                      setBannerDisplay(false);
                      setBannerBackgroundDisplay(false);
                    }}
                    class="btn btn-primary btn-documentation"
                  >
                    Image Type
                  </button>
                </div>
                <div className="col-md-3">
                  <button
                    type="button"
                    style={{ width: "100%" }}
                    onClick={() => {
                      setBannerType("background-img");
                      setBannerDisplay(false);
                      setBannerBackgroundDisplay(true);
                      setBackgroundEffect(false);
                    }}
                    class="btn btn-primary btn-documentation"
                  >
                    Background Type
                  </button>
                  {bannerBackgroundDisplay ? (
                    <>
                      <p style={{ margin: "0" }}>Effects:</p>

                      <button
                        type="button"
                        style={{ width: "100%" }}
                        onClick={() => {
                          setBannerDisplay(false);
                          setBackgroundEffect("parallax");
                        }}
                        class="btn btn-primary btn-documentation"
                      >
                        Parallax
                      </button>
                    </>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
              <div id="grids">
                <h1>Grids</h1>
                <div className="breaker"></div>

                <MediaGrid
                  location="/images/grids/demo/"
                  size={2}
                  columns="col-md-6"
                  imgType="jpg"
                />
              </div>
            </div>
          </Content>
        </div>
        <div className="col-md-2">
          <div className="documentation-sidebar">
            <div style={{ borderBottom: "1px solid grey" }}>
              <Content>
                <h4 style={{ margin: "0" }}>Contents</h4>
              </Content>
            </div>
            <Content>
              {headerLinks.map((link, id) => {
                var siftLink = slugify(link, { replacement: "", lower: true });
                return (
                  <a key={id} href={"#" + siftLink}>
                    {link}
                  </a>
                );
              })}
            </Content>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Documentation;
