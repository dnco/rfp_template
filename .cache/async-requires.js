// prefer default export if available
const preferDefault = m => (m && m.default) || m

exports.components = {
  "component---cache-dev-404-page-js": () => import("./../../dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-404-js": () => import("./../../../src/pages/404.js" /* webpackChunkName: "component---src-pages-404-js" */),
  "component---src-pages-broadgate-js": () => import("./../../../src/pages/broadgate.js" /* webpackChunkName: "component---src-pages-broadgate-js" */),
  "component---src-pages-documentation-js": () => import("./../../../src/pages/documentation.js" /* webpackChunkName: "component---src-pages-documentation-js" */),
  "component---src-pages-google-js": () => import("./../../../src/pages/google.js" /* webpackChunkName: "component---src-pages-google-js" */),
  "component---src-pages-index-js": () => import("./../../../src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */),
  "component---src-pages-secondpage-js": () => import("./../../../src/pages/secondpage.js" /* webpackChunkName: "component---src-pages-secondpage-js" */)
}

