const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/benswindells/Desktop/Web Development/Development Projects/rfp_template/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/benswindells/Desktop/Web Development/Development Projects/rfp_template/src/pages/404.js"))),
  "component---src-pages-broadgate-js": hot(preferDefault(require("/Users/benswindells/Desktop/Web Development/Development Projects/rfp_template/src/pages/broadgate.js"))),
  "component---src-pages-documentation-js": hot(preferDefault(require("/Users/benswindells/Desktop/Web Development/Development Projects/rfp_template/src/pages/documentation.js"))),
  "component---src-pages-google-js": hot(preferDefault(require("/Users/benswindells/Desktop/Web Development/Development Projects/rfp_template/src/pages/google.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/benswindells/Desktop/Web Development/Development Projects/rfp_template/src/pages/index.js"))),
  "component---src-pages-secondpage-js": hot(preferDefault(require("/Users/benswindells/Desktop/Web Development/Development Projects/rfp_template/src/pages/secondpage.js")))
}

