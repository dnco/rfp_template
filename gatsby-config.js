/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  siteMetadata: {
    title: `dn&co`,
    description: `We are dn&co, a brand and design consultancy inspired by culture and place.`,
    author: `dn&co`,
    links: ["Home", "Google", "Broadgate"],
  },

  plugins: [
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sass",
    "@babel/plugin-syntax-jsx",
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/app/*`] },
    },
    {
      resolve: "gatsby-plugin-exclude",
      options: { paths: ["/app/*", "!/documentation"] },
    },
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        // Available options and their defaults:
        base64Width: 20,
        forceBase64Format: ``, // valid formats: png,jpg,webp
        useMozJpeg: process.env.GATSBY_JPEG_ENCODER === `MOZJPEG`,
        stripMetadata: true,
        defaultQuality: 50,
      },
    },
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-css-literal-loader`,
      options: { extension: ".scss" },
    },
  ],
};
